from django.urls import path
from . import views
from django.contrib.auth.views import LogoutView

app_name = 'homepage'

urlpatterns = [
    path('', views.home, name='home'),
    path('log_in/', views.log_in, name='log_in'),
    path('sign_up/', views.sign_up, name='sign_up'),
    path('logout/', LogoutView.as_view(next_page='home'), name = 'logout')
]