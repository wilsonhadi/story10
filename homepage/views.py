from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm


# Create your views here.

def home(request):
    if(request.user):
        user = request.user
    response = {"user" : user}
    return render(request, 'home.html', response)

def log_in(request):
    return render(request, 'registration/login.html')

def sign_up(request):
    if request.method == "POST":
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/accounts/login/')

    form = UserCreationForm
    return render(request, 'registration/signup.html', context={"form":form})
