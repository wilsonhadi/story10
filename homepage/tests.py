from django.test import TestCase, Client
from django.apps import apps
from django.urls import resolve, reverse
from .views import *

from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User

# Create your tests here.

class test(TestCase):

    def test_home_url_exists_and_template_used(self):
        found = resolve('/')
        response = Client().get('/')
        self.assertEqual(found.func, home)
        self.assertEqual(response.status_code,200)
        self.assertTemplateUsed(response, 'home.html')
    
    def test_login_url_exists_and_template_used(self):
        found = resolve('/log_in/')
        response = Client().get('/log_in/')
        self.assertEqual(found.func, log_in)
        self.assertEqual(response.status_code,200)
        self.assertTemplateUsed(response, 'registration/login.html')

    def test_signup_url_exists_and_template_used(self):
        found = resolve('/sign_up/')
        response = Client().get('/sign_up/')
        self.assertEqual(found.func, sign_up)
        self.assertEqual(response.status_code,200)
        self.assertTemplateUsed(response, 'registration/signup.html')

    def test_form_register(self):
        response = self.client.post(reverse('homepage:sign_up'), data={'username': 'user', 'password1' : 'p1a2s3s4', 'password2' : 'p1a2s3s4'})
        count = User.objects.all().count()
        input = User.objects.all().first()
        self.assertEqual(input.username, 'user')
        self.assertEqual(count, 1)
